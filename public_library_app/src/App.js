import React from 'react';
import Home from "./components/Home";
import TabbedComponent from "./components/TabbedComponent";

function App() {
    return (
        <div className="App">
            <h1>Public Library System</h1>
            <TabbedComponent/>
        </div>
    );
}

export default App;
