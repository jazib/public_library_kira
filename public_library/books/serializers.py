from django.contrib.auth.models import User
from rest_framework import serializers, pagination
from .models import Book, UserReservedBook


class BookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ('pk', 'title', 'author', 'quantity')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username')


class ReservedBookSerializer(serializers.ModelSerializer):
    book = BookSerializer()
    user = UserSerializer()

    class Meta:
        model = UserReservedBook
        fields = ('pk', 'book', 'user', 'reserved_on')


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'first_name', 'last_name', 'email')


