from django.urls import path

from .views import customers_list, reserve_book, BooksList, ReservedBooksList

urlpatterns = [
    path('list/', BooksList.as_view(), name='bookslist'),
    path('reserve/', reserve_book, name='reserve'),
    path('reserve_list/', ReservedBooksList.as_view(), name='reserved_list'),
    path('customers/', customers_list),
]