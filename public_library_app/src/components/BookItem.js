import React, {Component} from "react";
import * as axios from "axios";
import {API_URL} from "../constants";
import {Button} from "reactstrap";
import Autocomplete from "react-autocomplete";

class BookItem extends Component {
    state = {
        isReserving: false,
        username: '',
        usersList: [],
        reservedSuccess: false,
    };

    onUserSearchChange = (e) => {
        axios.get(API_URL + `customers/?query=${this.state.username}`).then(res => this.setState({usersList: res.data}));
        this.setState({username: e.target.value})
    }

    onUserSelected = (val) => {
        this.setState({username: val})
    }
    onReserveClick = (e)  => {
        e.preventDefault();
        this.setState({
            isReserving: !this.state.isReserving
        })
    }

    onConfirmReserve = (e) => {
        const username = this.state.username;
        const body = {username: username, book_id: this.props.id}
        axios.post(API_URL + 'reserve/', body).then(res => this.setState({reservedSuccess: true}));
    }

    componentDidMount() {
    }


    render() {
        return (
            <div className="BookItem">
                <p>
                    {this.props.title} by {this.props.author}
                </p>
                <div>
                    <Button color="primary" onClick={this.onReserveClick}>Reserve</Button>
                    {this.state.isReserving &&
                    <div>
                        Enter customer's username to reserve
                        <Autocomplete
                            getItemValue={(item) => item.username}
                            getItemKey
                            items={this.state.usersList}
                            renderItem={(item, isHighlighted) =>
                                <div style={{background: isHighlighted ? 'lightgray' : 'white'}}>
                                    {item.username}
                                </div>
                            }
                            value={this.state.username}
                            onChange={this.onUserSearchChange}
                            onSelect={this.onUserSelected}
                        />

                        <Button color="primary" onClick={this.onConfirmReserve}>Confirm</Button>
                        {this.state.reservedSuccess && <p className='text-primary'>Reserved successfull!</p>}
                    </div>
                    }
                </div>
            </div>
        )
    }
}

export default BookItem;