from django.contrib.auth.models import User
from rest_framework import status, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from books.models import Book, UserReservedBook
from books.serializers import BookSerializer, CustomerSerializer, ReservedBookSerializer


class BooksList(generics.ListAPIView):
    serializer_class = BookSerializer

    def get_queryset(self):
        query = self.request.GET.get('query')
        if query:
            return Book.objects.filter(title__contains=query)
        else:
            return Book.objects.all()


class ReservedBooksList(generics.ListAPIView):
    serializer_class = ReservedBookSerializer

    def get_queryset(self):
        return UserReservedBook.objects.all()


@api_view(['POST'])
def reserve_book(request):
    if request.method == 'POST':
        username = request.data.get('username')
        book_id = request.data.get('book_id')
        if not username:
            return Response({'status': 'username is required'}, status=status.HTTP_400_BAD_REQUEST)
        if not book_id:
            return Response({'status': 'book_id is required'}, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.filter(username=username).first()
        user_reserved = UserReservedBook(user_id=user.id, book_id=book_id)
        user_reserved.save()
        return Response()


@api_view(['GET'])
def customers_list(request):
    if request.method == 'GET':
        query = request.GET.get('query')
        if query:
            users = User.objects.filter(username__contains=query)
        else:
            users = User.objects.all()
        serializer = CustomerSerializer(users, context={'request': request}, many=True)
        return Response(serializer.data)
