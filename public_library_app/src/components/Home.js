import React, {Component} from "react";
import * as axios from "axios";
import {API_URL} from "../constants";
import BookItem from "./BookItem";
import {InputGroup, InputGroupText, InputGroupAddon, ListGroup, Input, ListGroupItem, Button} from 'reactstrap';

class Home extends Component {
    state = {
        books: [],
        searchQuery: '',
        next: null,
    };

    componentDidMount() {
        this.resetState();
    }

    getBooks = () => {
        axios.get(API_URL + 'list').then(res => this.setState({books: res.data.results, next: res.data.next}));
    };

    resetState = () => {
        this.getBooks();
    };

    onSearchBooks = (e) => {
        axios.get(API_URL + `list/?query=${this.state.searchQuery}`).then(res => this.setState({
            books: res.data.results,
            next: res.data.next
        }));
    }

    onLoadMoreClick = (e) => {
        axios.get(this.state.next).then(res => {
            let data = res.data.results;
            let currentData = this.state.books;
            const newData = currentData.concat(data);
            this.setState({
                books: newData,
                next: res.data.next
            })
        } );
    }

    render() {
        const listItems = this.state.books.map((book) =>
            <ListGroupItem>
                <BookItem id={book['pk']}
                          title={book['title']}
                          author={book['author']}/>
            </ListGroupItem>
        );
        return (
            <div className="Home">
                 <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>Search for Books</InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="book name" value={this.state.searchQuery}
                    onChange={(e) => this.setState({searchQuery: e.target.value})}/>
                    <Button color="primary" onClick={this.onSearchBooks}>Go!</Button>
                  </InputGroup>
                <ListGroup>
                    {listItems}
                </ListGroup>
                {this.state.next && <Button color="secondary" onClick={this.onLoadMoreClick}>Load More</Button> }
            </div>
        )
    }
}

export default Home;