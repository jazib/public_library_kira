# Public Library
A simple Django + React application to allow listing, searching and reservation of books.

## Installation Instructions
### Backend Setup
1. Clone the Repository: `git clone https://jazib@bitbucket.org/jazib/public_library_kira.git`
2. `cd public_library_kira`
3. `cd public_library`
4. Create virtualenv and activate it. For this task, I used python3.6.  `virtualenv -p python3.6 venv && source venv/bin/activate`
5. Install dependencies: `pip install -r requirements.txt`
6. Run migrations: `python manage.py migrate` - For the purpose of this task, I am relying on default sqlite db
7. Load default data by using the custom management command included: `python manage.py bootstrap_data`

##### Running Instructions
1. Create superuser: `python manage.py createsuperuser` 
2. Run the server: `python manage.py runserver`
3. Go to the default django admin where you can now start adding some users `http://127.0.0.1:8000/admin`
4. Create some users using this admin panel as this is critical to later reserve books. 

### Frontend Setup
1.  Now in a new terminal, start with root(public_library_kira) folder.
2. `cd public_library_app`
3. `npm install`
4. `npm run start`
5. Open browser on `http:127.0.0.1:3000` and you should see app and default listing of books

Now you can start listing, searching and reserving the books! 

