import React, {Component} from "react";
import * as axios from "axios";
import {API_URL} from "../constants";
import {ListGroup, ListGroupItem} from "reactstrap";

class ReservedBooks extends Component {

    state = {
        books: [],
        next: null,
    }

    getReservedBooks = () => {
        axios.get(API_URL + 'reserve_list').then(res => this.setState({books: res.data.results, next: res.data.next}));
    }

    componentDidMount() {
        this.getReservedBooks();

    }

    render() {
        const listItems = this.state.books.map((book) =>
            <ListGroupItem>
                <div>
                    {book['book']['title']} by {book['book']['author']} reserved by {book['user']['username']}
                </div>
            </ListGroupItem>
        )

        return (
            <div>
                <h1> Reserved Books</h1>
                <ListGroup>
                    {listItems}
                </ListGroup>

            </div>
        );
    }
}

export default ReservedBooks;